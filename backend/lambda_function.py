import logging
import boto3
import os
import json

logger = logging.getLogger()

logger.setLevel(logging.INFO)

 

s3 = boto3.client('s3')
region_name=os.environ['region']
dynamodb = boto3.resource('dynamodb', region_name=region_name)



def storeInDB(tableName, content):
    table = dynamodb.Table(tableName)

    

    #convert binary to string
    binary_values = content.split()
    ascii_content = ""
    for binary_value in binary_values:
        an_integer = int(binary_value, 2)
        ascii_character = chr(an_integer)
        ascii_content += ascii_character

    form_upload = json.loads( ascii_content ) # This will parse the string into a native Python dictionary, be sure to add some error handling should a parsing error occur



    if ('firstname' in form_upload and 'lastname' in form_upload and 
    'email' in form_upload and 'birthdate' in form_upload and 
    'yearlyincome' in form_upload and 'yearlytax' in form_upload):
    #table.put_item(Item={'UserName':UserName,'Email':Email,'Score':Score,'Level':Level,'Mobile':Mobile,'Magic':Magic})
        if 'additionalinfo' in form_upload:
            table.put_item(TableName=tableName,
                Item={
                    'mailaddress':form_upload['email'],
                    'firstname':form_upload['firstname'],
                    'lastname':form_upload['lastname'],
                    'birthdate':form_upload['birthdate'],
                    'yearlyincome':form_upload['yearlyincome'],
                    'yearlytax':form_upload['yearlytax'],
                    'additionalinfo':form_upload['additionalinfo']
                }
            )
        else:
            table.put_item(TableName=tableName,
                Item={
                    'mailaddress':form_upload['email'],
                    'firstname':form_upload['firstname'],
                    'lastname':form_upload['lastname'],
                    'birthdate':form_upload['birthdate'],
                    'yearlyincome':form_upload['yearlyincome'],
                    'yearlytax':form_upload['yearlytax'],
                    'additionalinfo': 'null'
                }
            )
    

def lambda_handler(event, context):
    
    content = ''

    # retrieve bucket name and file_key from the S3 event

    bucket_name = event['Records'][0]['s3']['bucket']['name']

    file_key = event['Records'][0]['s3']['object']['key']

    logger.info('Reading {} from {}'.format(file_key, bucket_name))

    # get the object

    obj = s3.get_object(Bucket=bucket_name, Key=file_key)

    # get lines inside the csv

    lines = obj['Body'].read().split(b'\n')

    for r in lines:
       content = content + r.decode()

    #Initialise the Helper object and check whether to create a new page or update existing one

    storeInDB(os.environ['tableName'], content)
    