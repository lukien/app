import './App.css';

import React from "react";

const AWS = require('aws-sdk');

require('dotenv').config();

var bucketName = process.env.REACT_APP_BUCKET_NAME;
var bucketRegion = process.env.REACT_APP_REGION;
var IdentityPoolId = process.env.REACT_APP_IDENTITY_POOL_ID;








class MessageBody {
  constructor(firstname, lastname, email, birthdate,
    svn, yearlyincome, yearlytax, additionalinfo) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.birthdate = birthdate;
    this.svn = svn;
    this.yearlyincome = yearlyincome;
    this.yearlytax = yearlytax;
    this.additionalinfo = additionalinfo;
  }
}

export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {};
    this.state.color = 'black';

    this.handleSubmit = this.handleSubmit.bind(this);

  }
  handleFirstNameChange = (event) => {
    this.setState({ 'firstname': event.target.value });
  }

  handleLastNameChange = (event) => {
    this.setState({ 'lastname': event.target.value });
  }

  handleEmailChange = (event) => {

    this.setState({ 'email': event.target.value });
    if (!this.validateEmail(event.target.value)) {
      this.setState({ color: 'red' })
      this.setState({ notValidEmail: "Not valid email" });
    } else {
      this.setState({ color: 'black' })
      this.setState({ notValidEmail: "" });
    }

  }

  handleBirthDateChange = (event) => {
    this.setState({ 'birthdate': event.target.value });
  }

  handleYearlyIncomeChange = (event) => {
    this.setState({ 'yearlyincome': event.target.value });
  }

  handleYearlyTaxChange = (event) => {
    this.setState({ 'yearlytax': event.target.value });
  }

  handleAdditionalInfoChange = (event) => {
    this.setState({ 'additionalinfo': event.target.value });
  }

  validateEmail(email) {
    const re = /^(([^<>()\]\\.,;:\s@"]+(\.[^<>(\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }

  handleSubmit(e) {
    if (this.state.color === 'red') {
      return;
    } else {

      if (!this.state.firstname ||
        !this.state.lastname ||
        !this.state.email ||
        !this.state.birthdate ||
        !this.state.yearlyincome ||
        !this.state.yearlytax) {
        return
      }

      var messageBody = new MessageBody(this.state.firstname,
        this.state.lastname, this.state.email,
        this.state.birthdate, this.state.svn,
        this.state.yearlyincome, this.state.yearlytax,
        this.state.additionalinfo);

        

      this.uploadFileToS3(this.text2Binary(JSON.stringify(messageBody)));
    }
  };


  text2Binary(string) {
    return string.split('').map(function (char) {
        return char.charCodeAt(0).toString(2);
    }).join(' ');
  };

  uploadFileToS3(elem) {
    AWS.config.update({
      region: bucketRegion,
      credentials: new AWS.CognitoIdentityCredentials({
        IdentityPoolId: IdentityPoolId
      })
    });

    
    var fileName = "formupload/form_" + Date.now() + ".txt";

    // Use S3 ManagedUpload class as it supports multipart uploads
    var upload = new AWS.S3.ManagedUpload({
      params: {
        Bucket: bucketName,
        Key: fileName,
        Body: elem
      }
    });

    var promise = upload.promise();

    promise.then(
      function (data) {
        alert("Successfully uploaded file.");
      },
      function (err) {
        console.log(err);
        return alert("There was an error uploading the file: ", err.message);
      }
    );
  }


  render() {
    return (<div className="form-style-5" >
        <fieldset >
          <legend >
            <span className="number" > 1 </span>
            Candidate Info </legend>
          <label htmlFor="firstname" > Your Firstname </label> <
            input type="text"
            name="firstName"
            onChange={this.handleFirstNameChange}
            required />
          <label htmlFor="lastname" >
            Your Lastname </label> <
            input type="text"
            name="lastName"
            onChange={this.handleLastNameChange}
            required />
          <div >
            < label htmlFor="email" >
              Your Email </label> <span style={{ color: this.state.color }
            } > {this.state.notValidEmail} </span> <
              input type="email"
              name="email"
              onChange={this.handleEmailChange}
              required />
          </div> <label htmlFor="birthdate" >
            Your Birthdate </label> <
            input type="date"
            name="birthdate"
            onChange={this.handleBirthDateChange}
            required />
          <label htmlFor="svn" >
            Your Social Security Number </label> <
            input type="text"
            name="svn"
            required />
          < label htmlFor="yearlyincome" >
            Your Yearly Income </label> <
            input type="number"
            name="yearlyincome"
            onChange={this.handleYearlyIncomeChange}
            required />
          < label htmlFor="yearlytax" >
            Your Yearly Tax </label> <
            input type="number"
            name="yearlytax"
            onChange={this.handleYearlyTaxChange}
            required />
        </fieldset>
        <fieldset >
          <  legend >
            < span className="number" > 2 </span> Additional Info</legend >
          <textarea name="additionalinfo"
            onChange={this.handleAdditionalInfoChange}
            placeholder="Additional notes" >
          </textarea>
        </fieldset >
        < button type="submit"
          onClick={this.handleSubmit} > Einloggen!
        </button>
      </div>
    );
  }
}